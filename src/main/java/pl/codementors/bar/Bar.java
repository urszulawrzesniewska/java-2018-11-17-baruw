package pl.codementors.bar;

public class Bar {

    private String drink = null;

    public synchronized void put() {
        this.drink = drink;
        notifyAll();
    }


    public synchronized String take() throws InterruptedException {
        while (drink == null) {
            this.wait();
        }
        String ret = drink;
        drink = null;
        return ret;
    }
}
