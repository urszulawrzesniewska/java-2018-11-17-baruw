package pl.codementors.bar;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Client implements Runnable {
    private String name;
    private static final Logger log = Logger.getLogger(Bartender.class.getCanonicalName());
    private boolean running = true;
    private Bar drink;


    public Client(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        while (running) {
            try {
                String res = drink.take();
                System.out.println(drink + ": " + res);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }
}
