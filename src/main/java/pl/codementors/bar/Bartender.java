package pl.codementors.bar;

import java.util.Scanner;
import java.util.logging.Logger;

public class Bartender implements Runnable {
    private String name;
    private static final Logger log = Logger.getLogger(Bartender.class.getCanonicalName());
    private boolean running = true;
    private Bar drink;

    public Bartender(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line.equals("quit")) {
                break;
            }
            drink.put();
        }
    }
    public void stop() {
        running = false;
    }
}
