package pl.codementors.bar;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        startClient();
        startBartender();
    }
    public static void readDrinks() throws IOException {
        List<String> drinks = Files.readAllLines(Paths.get("/home/student/bar/Drinks"));
    }
    private static void startClient() {
        Client client1 = new Client("Bob");
        Client client2 = new Client("Kate");
        Client client3 = new Client("Jack");
        Client client4 = new Client("Joanne");
        Client client5 = new Client("Johnny");


        new Thread(client1).start();
        new Thread(client2).start();
        new Thread(client3).start();
        new Thread(client4).start();
        new Thread(client5).start();
    }
    private static void startBartender() {
        Bartender bartender = new Bartender("Kurt");

    }
}
